﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive;
using System.Threading;
using System.Threading.Tasks;
using ReactiveUI;

namespace AvaloniaApplication1.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    public string? NumerosParaMedia
    {
        get => _numerosParaMedia;
        set => this.RaiseAndSetIfChanged(ref _numerosParaMedia, value);
    }

    public string? CronometroSequencialString
    {
        get => _cronometroSequencialString;
        set => this.RaiseAndSetIfChanged(ref _cronometroSequencialString, value);
    }
    
    public string? CronometroSimultaneoString
    {
        get => _cronometroSimultaneoString;
        set => this.RaiseAndSetIfChanged(ref _cronometroSimultaneoString, value);
    }
    public int QuantidadeDeNumeros
    {
        get;
        set;
    }

    public float MediaPonderadaSequencial
    {
        get => _mediaPonderadaSequencial;
        set => this.RaiseAndSetIfChanged(ref _mediaPonderadaSequencial, value);
    }
    
    public float MediaPonderadaSimultanea
    {
        get => _mediaPonderadaSimultanea;
        set => this.RaiseAndSetIfChanged(ref _mediaPonderadaSimultanea, value);
    }
    
    public List<int> Sorteados = new();
    private string? _numerosParaMedia;
    private float _mediaPonderadaSequencial;
    private string? _cronometroSequencialString;
    private string? _cronometroSimultaneoString;
    private float _mediaPonderadaSimultanea;
    public ReactiveCommand<Unit, Unit> SortearCommand { get; set; }
    public ReactiveCommand<Unit, Unit> SequencialmenteCommand { get; set; }
    public ReactiveCommand<Unit, Unit> SimultaneamenteCommand { get; set; }

    public MainWindowViewModel()
    {
        SortearCommand = ReactiveCommand.Create(Sortear);
        SequencialmenteCommand = ReactiveCommand.Create(Sequencial);
        SimultaneamenteCommand = ReactiveCommand.Create(Simultaneo);
    }

    private void Sortear()
    {
        Sorteados.Clear();
        Random random = new Random();
        
        for (int i = 0; i < QuantidadeDeNumeros; i++)
        {
            int numeroSorteado = random.Next(0, 11);
            Sorteados.Add(numeroSorteado);
        }

        NumerosParaMedia = string.Join(", ", Sorteados);
    }

    private void Sequencial()
    {
        if (Sorteados.Count <= 0) return;
        Stopwatch cronometroSequencial = Stopwatch.StartNew();
        Dictionary<int, int> repeticoes = new Dictionary<int, int>();
        
        foreach (var numero in Sorteados)
        {
            if (repeticoes.ContainsKey(numero))
            {
                repeticoes[numero]++;
            }
            else
            {
                repeticoes[numero] = 1;
            }

            Thread.Sleep(1); //Simulando um trabalho mais demorado/complexo
        }
        
        float numeradorDaMediaPonderada = 0;
        foreach (var par in repeticoes)
        {
            numeradorDaMediaPonderada += par.Key * par.Value;
            Thread.Sleep(1); //Simulando um trabalho mais demorado/complexo
        }

        float denominadorDaMediaPonderada = 0;
        foreach (var par in repeticoes)
        {
            denominadorDaMediaPonderada += par.Value;
            Thread.Sleep(1); //Simulando um trabalho mais demorado/complexo
        }
        
        if (denominadorDaMediaPonderada != 0)
        {
            MediaPonderadaSequencial = numeradorDaMediaPonderada / denominadorDaMediaPonderada;
            Thread.Sleep(1); //Simulando um trabalho mais demorado/complexo
        }
        cronometroSequencial.Stop();
        CronometroSequencialString = $"Feito em: {cronometroSequencial.ElapsedMilliseconds} ms";
    }

    public void Simultaneo()
    {
        if (Sorteados.Count <= 0) return;

        Stopwatch cronometroSimultaneo = Stopwatch.StartNew();

        Dictionary<int, int> repeticoes = new Dictionary<int, int>();
        
        Parallel.ForEach(Sorteados, (numero) =>
        {
            lock (repeticoes)
            {
                if (repeticoes.ContainsKey(numero))
                {
                    repeticoes[numero]++;
                }
                else
                {
                    repeticoes[numero] = 1;
                }
            }

            Thread.Sleep(1); // Simula um trabalho mais demorado/complexo
        });

        int numeradorDaMediaPonderada = 0;
        
        Parallel.ForEach(repeticoes, (par) =>
        {
            Interlocked.Add(ref numeradorDaMediaPonderada, par.Key * par.Value);
            Thread.Sleep(1); // Simula um trabalho mais demorado/complexo
        });

        int denominadorDaMediaPonderada = 0;
        
        Parallel.ForEach(repeticoes, (par) =>
        {
            Interlocked.Add(ref denominadorDaMediaPonderada, par.Value);
            Thread.Sleep(1); // Simula um trabalho mais demorado/complexo
        });
        
        if (denominadorDaMediaPonderada != 0)
        {
            MediaPonderadaSimultanea = (float) numeradorDaMediaPonderada / denominadorDaMediaPonderada;
            Thread.Sleep(1); // Simula um trabalho mais demorado/complexo
        }

        cronometroSimultaneo.Stop();
        CronometroSimultaneoString = $"Feito em: {cronometroSimultaneo.ElapsedMilliseconds} ms";
    }
    
}